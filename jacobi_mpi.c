/************************************************************
 * Program to solve a finite difference
 * discretization of the screened Poisson equation:
 * (d2/dx2)u + (d2/dy2)u - alpha u = f
 * with zero Dirichlet boundary condition using the iterative
 * Jacobi method with overrelaxation.
 *
 * RHS (source) function
 *   f(x,y) = -alpha*(1-x^2)(1-y^2)-2*[(1-x^2)+(1-y^2)]
 *
 * Analytical solution to the PDE
 *   u(x,y) = (1-x^2)(1-y^2)
 *
 * Current Version: Christian Iwainsky, RWTH Aachen University
 * MPI C Version: Christian Terboven, RWTH Aachen University, 2006
 * MPI Fortran Version: Dieter an Mey, RWTH Aachen University, 1999 - 2005
 * Modified: Sanjiv Shah,        Kuck and Associates, Inc. (KAI), 1998
 * Author:   Joseph Robicheaux,  Kuck and Associates, Inc. (KAI), 1998
 *
 * Unless READ_INPUT is defined, a meaningful input dataset is used (CT).
 *
 * Input : n     - grid dimension in x direction
 *         m     - grid dimension in y direction
 *         alpha - constant (always greater than 0.0)
 *         tol   - error tolerance for the iterative solver
 *         relax - Successice Overrelaxation parameter
 *         mits  - maximum iterations for the iterative solver
 *
 * On output
 *       : u(n,m)       - Dependent variable (solution)
 *       : f(n,m,alpha) - Right hand side function
 *
 *************************************************************/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <mpi.h>


#define TRUE 1
#define FALSE 0


#define EAST 0
#define WEST 1
#define NORTH 2
#define SOUTH 3

#define HERE 4


MPI_Datatype column, row, matrix;
MPI_Comm cart_comm;

int *GetDims(int tam);
int *GetSize (int total, int nums, int pos);

void print_vec(double *vec, int size);
void print(double *mat,
           int startX, int endX,
           int startY, int endY,
           int maxXCount, int maxYCount
           );

void fill_mat(double *mat,
              int startX, int endX,
              int startY, int endY,
              int maxXCount, int maxYCount,
              double val
              );

int FillBorder(double *mat,
               int num_N, int num_M, int viz[4],
int maxXCount, int maxYCount);

/*************************************************************
 * Performs one iteration of the Jacobi method and computes
 * the residual value.
 *
 * NOTE: u(0,*), u(maxXCount-1,*), u(*,0) and u(*,maxYCount-1)
 * are BOUNDARIES and therefore not part of the solution.
 *************************************************************/
double one_jacobi_iteration(double xStart, double yStart,
                            int offX, int offY,
                            int maxXCount, int maxYCount,
                            double *src, double *dst,
                            double deltaX, double deltaY,
                            double alpha, double omega)
{

#define SRC(YY,XX) src[(XX)*maxXCount+(YY)]
#define DST(XX,YY) dst[(XX)*maxXCount+(YY)]

    int x, y;

    double fX, fY;
    double error = 0.0;

    double updateVal;
    double f;

    // Coefficients
    double cx = 1.0 / (deltaX * deltaX);
    double cy = 1.0 / (deltaY * deltaY);
    double cc = (-2.0 * cx) - (2.0 * cy) - alpha;



    for (y = 1; y < (maxYCount - 1); y++) {

        fY = yStart + (y + offY - 1) * deltaY;
        //        printf("> y: [%d] -> fY(%f) = [%f | %f])\n", y + offY, fY, yStart, deltaY);

        for (x = 1; x < (maxXCount - 1); x++) {
            fX = xStart + (x + offX - 1) * deltaX;
            //          printf("> x:  [%d] -> fX(%f) = [%f | %f])\n", x + offX, fX, xStart, deltaX);

            //       printf("> (%d, %d) off (%d, %d)\n", x, y, offX, offY);

            f = -alpha * (1.0 - fX * fX) * (1.0 - fY * fY)
                    -2.0 * (1.0 - fX * fX) -2.0 * (1.0 - fY * fY);

            //         printf("> f: %f \n", f);

            updateVal = ((SRC(x - 1, y) + SRC(x + 1, y)) * cx +
                         (SRC(x, y - 1) + SRC(x, y + 1)) * cy +
                         SRC(x, y) * cc - f) / cc;

            //        printf("> up: %f \n", updateVal);

            //        printf("(%d, %d)\n", x, y);
            DST(y,x) = SRC(x,y) - omega * updateVal;
            error += updateVal * updateVal;

            //        printf("(%f,%f)\n", fX, fY);

            //        printf("%f ", SRC(x-1,y));
            //        printf("%f ", SRC(x+1,y));
            //        printf("%f ", SRC(x,y-1));
            //        printf("%f ", SRC(x,y+1));
            //        printf("\n");

            //  printf(">[%d, %d] -> %f , err = %f\n", x, y, updateVal, error);
        }
    }
    //  printf("\n");
    return error;
}



/**********************************************************
 * Checks the error between numerical and exact solutions
 **********************************************************/
double checkSolution(double xStart, double yStart,
                     int maxXCount, int maxYCount,
                     double *u,
                     double deltaX, double deltaY,
                     double alpha)
{
#define U(YY,XX) u[(XX)*maxXCount+(YY)]
    int x, y;
    double fX, fY;
    double localError, error = 0.0;

    for (y = 1; y < (maxYCount-1); y++)
    {
        fY = yStart + (y-1)*deltaY;
        for (x = 1; x < (maxXCount-1); x++)
        {
            fX = xStart + (x-1)*deltaX;
            localError = U(x,y) - (1.0-fX*fX)*(1.0-fY*fY);
            error += localError * localError;
        }
    }
    return sqrt(error)/((maxXCount-2)*(maxYCount-2));
}


MPI_Comm cart_comm;

int main(int argc, char **argv)
{
    int n, m, mits;

    double alpha, tol, relax;
    double maxAcceptableError;

    double sub_error, error;
    double *u, *tmp;
    double *sub_u, *sub_u_old;

    double t1, t2;
    t1 = t2 = 0.0;

    int allocCount;
    int iterationCount, maxIterationCount;

    MPI_Init(&argc, &argv);


    /* Process rank and number of processes */

    int pid, ntasks;
    MPI_Comm_rank(MPI_COMM_WORLD, &pid);
    MPI_Comm_size(MPI_COMM_WORLD, &ntasks);

    if(pid == 0) {
        t1 = MPI_Wtime();
    }

    /* Cartesian coordenates */

    int coords[2];
    int ndims = 2, *dims = GetDims(ntasks);
    int periods[2] = { FALSE, FALSE }, reorder = TRUE;

    MPI_Cart_create(MPI_COMM_WORLD, ndims, dims, periods, reorder, &cart_comm);
    MPI_Cart_coords(cart_comm, pid, ndims, coords);

    /* Submatrix Datatype */

    MPI_Datatype *submatrix = calloc(ntasks, sizeof(MPI_Datatype));

#ifdef READ_INPUT
    if (!pid) {
        printf("Input n,m - grid dimension in x,y direction:\n");
        scanf("%d,%d", &n, &m);

        printf("Input alpha - Helmholts constant:\n");
        scanf("%lf", &alpha);

        printf("Input relax - Successive over-relaxation parameter:\n");
        scanf("%lf", &relax);

        printf("Input tol - error tolerance for iterrative solver:\n");
        scanf("%lf", &tol);

        printf("Input mits - Maximum iterations for solver:\n");
        scanf("%d", &mits);
    }
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&alpha, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&relax, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&mits, 1, MPI_INT, 0, MPI_COMM_WORLD);
#else
    n = 4;
    m = 8;
    alpha = 0.8;
    relax = 1.0;
    tol = 1e-7;
    mits = 10;
#endif

    if (!pid)
        printf("-> %d, %d, %g, %g, %g, %d\n", n, m, alpha, relax, tol, mits);

    int sizeN = n + 2;
    int sizeM = m + 2;

    allocCount = (sizeN) * (sizeM);

    if (!pid) {
        // Those two calls also zero the boundary elements
        u = calloc(allocCount, sizeof(double));

        // fill_mat(u, 0, sizeX, 0, sizeY, sizeX, sizeY, 32.0);

        if (u == NULL)
        {
            fprintf(stderr, "Not enough memory for two %ix%i matrices\n", n+2, m+2);
            exit(1);
        }
    } else {
        u = NULL;
    }

    maxIterationCount = mits;
    maxAcceptableError = tol;


    // Solve in [-1, 1] x [-1, 1]
    double xLeft = -1.0, xRight = 1.0;
    double yBottom = -1.0, yUp = 1.0;

    double deltaX = (xRight-xLeft) / (n-1);
    double deltaY = (yUp-yBottom) / (m-1);

    iterationCount = 0;
    error = HUGE_VAL;


    /* sub-Matrix allocation */

    //    printf("%d <-> %d\n", coords[0], coords[1]);
    //    printf("%d !-! %d\n", dims[0], dims[1]);

    int *sub_m = GetSize(m, dims[0], coords[0]);
    int *sub_n = GetSize(n, dims[1], coords[1]);

    int sub_size_n = sub_n[1] - sub_n[0];
    int sub_size_m = sub_m[1] - sub_m[0];

    int sub_border_n = sub_size_n + 2;
    int sub_border_m = sub_size_m + 2;

    int sub_alloc = (sub_border_n) * (sub_border_m);

    sub_u = calloc(sub_alloc, sizeof(double));
    sub_u_old = calloc(sub_alloc, sizeof(double));


    /* Sub-Matriz for each process */

    MPI_Request *reqs = calloc(ntasks, sizeof(MPI_Request));

    if (!pid) {

        int i;
        int coords[2];

        for (i = 0; i < ntasks; i++) {

            MPI_Cart_coords(cart_comm, i, ndims, coords);

            int *local_m = GetSize(m, dims[0], coords[0]);
            int *local_n = GetSize(n, dims[1], coords[1]);

            //printf("n,m: %d %d\n", local_, local_m);

            int local_size_n = local_n[1] - local_n[0];
            int local_size_m = local_m[1] - local_m[0];

            //printf("size n,m: %d %d\n", local_size_n, local_size_m);

            int subsizes[2] = { local_size_m, local_size_n};
            int starts[2] = { local_m[0] + 1, local_n[0] + 1 };

            int sizes[2] = { m + 2, n + 2 };

            MPI_Type_create_subarray(ndims, sizes, subsizes,
                                     starts, MPI_ORDER_C, MPI_DOUBLE, &submatrix[i]);
            MPI_Type_commit(&submatrix[i]);

//            printf("%d %d - %d %d - %d %d\n",
//                   starts[1], starts[1] + subsizes[1],
//                    starts[0], starts[0] + subsizes[0],
//                    sizeN, sizeM);

//                                    fill_mat(u,
//                                             starts[1], starts[1] + subsizes[1],
//                                            starts[0], starts[0] + subsizes[0],
//                                            sizeN, sizeM,
//                                            i + 1);

            //MPI_Send(u, 1, submatrix[i], i, 1, MPI_COMM_WORLD);
            MPI_Isend(u, 1, submatrix[i], i, 1, MPI_COMM_WORLD, &reqs[i]);
        }
        //print(u, 0, sizeN, 0, sizeM, sizeN, sizeM);
        //print_vec(u, allocCount);
    }


#define SUB_U(XX,YY) sub_u[(XX)*sub_border_n+(YY)]
#define SUB_U_OLD(XX,YY) sub_u_old[(XX)*sub_border_n+(YY)]

    /* Set new row datatype */
    MPI_Type_contiguous(sub_size_n, MPI_DOUBLE, &row);
    MPI_Type_commit(&row);

    /* Set new column datatype */
    MPI_Type_vector(sub_size_m, 1, sub_border_n, MPI_DOUBLE, &column);
    MPI_Type_commit(&column);

    /* Set new matrix datatype */
    MPI_Type_vector(sub_size_m, sub_size_n, sub_border_n, MPI_DOUBLE, &matrix);
    MPI_Type_commit(&matrix);

    MPI_Recv(&SUB_U(1, 1), 1, matrix, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if (!pid)
        MPI_Waitall(ntasks, reqs, MPI_STATUSES_IGNORE);

    int viz[4];
    MPI_Cart_shift(cart_comm, 0, 1, &viz[NORTH], &viz[SOUTH]);
    MPI_Cart_shift(cart_comm, 1, 1, &viz[WEST], &viz[EAST]);


    /**********************************************************
     * Iterate as long as it takes to meet the convergence criterion
     **********************************************************/
    while (iterationCount < maxIterationCount && error > maxAcceptableError)
    {
        FillBorder(sub_u_old, sub_size_n, sub_size_m, viz, sub_border_n, sub_border_m);

        sub_error = one_jacobi_iteration(xLeft, yBottom,
                                         sub_n[0], sub_m[0],
                sub_border_n, sub_border_m,
                sub_u_old, sub_u,
                deltaX, deltaY,
                alpha, relax);


        MPI_Reduce(&sub_error, &error,  1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        if(!pid){
            printf("Iteration %i\n", iterationCount);

            error = sqrt(error) / (n * m);
            printf("\tError %g\n", error);
        }

        MPI_Bcast(&error, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        //  print(sub_u, 0, sub_border_n , 0, sub_border_m, sub_border_n, sub_border_m);
        //    print_vec(sub_u, sub_alloc);

        iterationCount++;
        // Swap the buffers

        tmp = sub_u_old;
        sub_u_old = sub_u;
        sub_u = tmp;

//        printf("SUBU !!!\n");
        // print(sub_u, 0, sub_border_n , 0, sub_border_m, sub_border_n, sub_border_m);
        //printf("SUBU_OLD !!!\n");
//        print(sub_u_old, 0, sub_border_n , 0, sub_border_m, sub_border_n, sub_border_m);
        //    print_vec(sub_u, sub_alloc);

    }

    MPI_Request q = MPI_REQUEST_NULL;
    MPI_Isend(&SUB_U_OLD(1, 1), 1, matrix, 0, 2, MPI_COMM_WORLD, &q);

    if(!pid) {
        int i;
        for (i = 0; i < ntasks; i++) {
            MPI_Irecv(u, 1, submatrix[i], i, 2,  MPI_COMM_WORLD, &reqs[i]);
        }

        MPI_Waitall(ntasks, reqs, MPI_STATUSES_IGNORE);

//        int j;
//        for (j = 0; j < allocCount; ++j) {
//            if ((j % (n + 2)) == 0)
//                printf("\n");
//            printf("%4.2f ", u[j]);

//        }
//        printf("\n");


      //  print(u, 0, sizeN, 0, sizeM, sizeN, sizeM);
        printf("Residual %g\n",error);

        // u_old holds the solution after the most recent buffers swap
        double absoluteError = checkSolution(xLeft, yBottom,
                                             n + 2, m + 2,
                                             u,
                                             deltaX, deltaY,
                                             alpha);



        printf("The error of the iterative solution is %g\n", absoluteError);
    }

    MPI_Wait(&q, MPI_STATUS_IGNORE);

    if(pid == 0) {
        t2 = MPI_Wtime();
        printf("Wall Time : %f\n", t2 - t1);
    }

    MPI_Finalize();

    return 0;
}


#define MAT(XX,YY) mat[(XX)*maxXCount+(YY)]

/**********************************************************
 * Fill de borders using catesian coords
 **********************************************************/

int FillBorder(double *mat, int num_N, int num_M, int viz[4], int maxXCount, int maxYCount)
{
    int err = 0;
    /* Request Handlers */
    MPI_Request reqs[8] = {
        MPI_REQUEST_NULL, MPI_REQUEST_NULL,
        MPI_REQUEST_NULL, MPI_REQUEST_NULL,
        MPI_REQUEST_NULL, MPI_REQUEST_NULL,
        MPI_REQUEST_NULL, MPI_REQUEST_NULL
    };

    /* Set column pointer*/
    double *WE_send = &MAT(1,1);
    double *EA_send = &MAT(1,num_N);

    double *EA_recv = &MAT(1,num_N+1);
    double *WE_recv = &MAT(1,0);

    /* Set row pointers */
    double *SO_send = &MAT(num_M,1);
    double *NO_send = &MAT(1,1);

    double *NO_recv = &MAT(0,1);
    double *SO_recv = &MAT(num_M+1,1);

    /* Eastward and westward data shifts. */
    MPI_Isend(WE_send, 1, column, viz[WEST], 0, cart_comm, &reqs[0]);
    MPI_Isend(EA_send, 1, column, viz[EAST], 1, cart_comm, &reqs[1]);

    MPI_Irecv(EA_recv, 1, column, viz[EAST], 0, cart_comm, &reqs[2]);
    MPI_Irecv(WE_recv, 1, column, viz[WEST], 1, cart_comm, &reqs[3]);

    /* Northward and southward data shifts. */
    MPI_Isend(SO_send, 1, row, viz[SOUTH], 2, cart_comm, &reqs[4]);
    MPI_Isend(NO_send, 1, row, viz[NORTH], 3, cart_comm, &reqs[5]);

    MPI_Irecv(NO_recv, 1, row, viz[NORTH], 2, cart_comm, &reqs[6]);
    MPI_Irecv(SO_recv, 1, row, viz[SOUTH], 3, cart_comm, &reqs[7]);

    /* Synchronize all Send and Recvs */
    MPI_Waitall(8, reqs, MPI_STATUSES_IGNORE);

    return err;
}


/**********************************************************
 * Get the local matrix dimensions for each process
 **********************************************************/
int *GetDims(int tam)  {

    int *divs = malloc(tam * sizeof(int));
    int *res = malloc(2 * sizeof(int));
    int i, j = 0;

    /* Get divisibles by 'tam' */
    for (i = 1; i <= tam; i++) {
        if (tam % i == 0) {
            divs[j] = i;
            j++;
        }
    }
    /* Median (most evenly distributed) of the divisibles  */
    res[0] = res[1] = divs[j/2];
    /* not perfect square*/
    if(j % 2 == 0)
        res[1] = divs[j/2 - 1];

    //  free(divs);

    return res;
}

/**********************************************************
 * Get location of each proccess in the World Matrix
 **********************************************************/
int *GetSize (int total, int nums, int pos)
{
    /* Array with begin and end*/
    int *res = malloc(2 * sizeof(int));
    res[1] = total / nums;

    int mod = total % nums;
    if (pos < mod) {
        res[0] = pos * (res[1] + 1);
        res[1]++;
    } else {
        res[0] = mod * (res[1] + 1) + (pos - mod) * res[1];
    }
    res[1] = res[0] + res[1];
    return res;
}



void print(double *mat, int startX, int endX, int startY, int endY, int maxXCount, int maxYCount)
{
    printf("--------------\n");
    int i, j;
    for (i = startY; i < endY; ++i) {
        for (j = startX; j < endX; ++j) {
            printf("%5.2f ", MAT(i, j));
        }
        printf("\n");
    }
    printf("--------------\n");
}

void print_vec(double *vec, int size)
{
    int i;
    for (i = 0; i < size; ++i) {
        printf("%5.2f ", vec[i]);
    }
    printf("\n");
}

void fill_mat(double *mat, int startX, int endX, int startY, int endY, int maxXCount, int maxYCount, double val)
{
    int i, j;
    for (i = startY; i < endY; ++i) {
        for (j = startX; j < endX; ++j) {
            MAT(i, j) = val;
        }
    }
}
